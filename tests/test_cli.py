from click.testing import CliRunner

import hja_hobo_clean
from hja_hobo_clean.cli import main

def test_version():
    runner = CliRunner()
    result = runner.invoke(main, ['--version'])
    assert result.exit_code == 0
    assert hja_hobo_clean.__version__ in result.output
