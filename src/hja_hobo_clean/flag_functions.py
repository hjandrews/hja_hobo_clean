# Each of these functions should take a pandas DataFrame and return a series
# identifying a binary determination of whether or not the flag was met
from datetime import timedelta
import numpy as np
import pandas as pd


def time_gap(df, args):
    """
    Track when a record is not exactly one of the specified intervals
    apart from its previous/next records
    """
    time_delta = df.DATE_TIME.diff()
    conds = (time_delta.notnull())
    for x in args.intervals:
        conds &= (time_delta != pd.Timedelta(minutes=x))
    return np.where(conds, 1, 0)


def extreme_temperature(df, args):
    """
    Track when a record temperature is outside the acceptable temperature
    range
    """
    conds = (df.TEMP_C > args.max_temp) | (df.TEMP_C < args.min_temp)
    return np.where(conds, 1, 0)


def high_intensity(df, args):
    """
    Track when a record light intensity is outside the acceptable range
    """
    conds = (df.INTENSITY > args.max_intensity)
    return np.where(conds, 1, 0)


def low_daylight_intensity(df, args):
    """
    Track when a record light intensity is abnormally low during daytime
    """
    # Find if this record is close to a sunrise/sunset edge based on the 
    # record_window argument.  If within this window, call this a nighttime
    # record so that it doesn't get flagged
    daylight = df.DAYLIGHT.rolling(
            window=2*args.record_window+1, min_periods=1, center=True
    ).min()

    conds = (
        (daylight == 1) &
        (df.INTENSITY <= args.min_intensity)
    )
    return np.where(conds, 1, 0)


def nighttime_intensity(df, args):
    """
    Track when a record light intensity is present during nighttime
    """
    # Find if this record is close to a sunrise/sunset edge based on the 
    # record_window argument.  If within this window, call this a daytime 
    # record so that it doesn't get flagged
    daylight = df.DAYLIGHT.rolling(
            window=2*args.record_window+1, min_periods=1, center=True
    ).max()

    conds = (
        (daylight == 0) &
        (df.INTENSITY >= args.max_intensity)
    )
    return np.where(conds, 1, 0)


def snow(df, args):
    """
    Track when series of records is at near-zero for a certain length
    """
    df = df.copy()

    # First, median filter the temperatures to avoid small spikes in the data
    df['TEMP_C'] = df.TEMP_C.rolling(
            window=7, min_periods=1, center=True).median()

    # Flag all temperature measurements when within the defined range
    conds = (
        (df.TEMP_C >= args.min_temperature) &
        (df.TEMP_C <= args.max_temperature)
    )
    df['NEAR_ZERO'] = np.where(conds, 1, 0)

    # Identify boundaries between inclusion in the NEAR_ZERO range as groups
    # Each time a measurement differs from its neighbor, a new group is
    # created by calculating a cumulative sum
    df['GROUP'] = (df.NEAR_ZERO.diff(1) != 0).astype('int').cumsum()

    # Summarize the groups and track the time delta between endpoints
    group_df = pd.DataFrame({
        'FIRST_DATE': df.groupby('GROUP').DATE_TIME.first(),
        'LAST_DATE': df.groupby('GROUP').DATE_TIME.last(),
        'GROUP': df.groupby('GROUP').GROUP.first(),
        'NEAR_ZERO': df.groupby('GROUP').NEAR_ZERO.first(),
    }).reset_index(drop=True)
    group_df['DIFF_DAYS'] = (
        (group_df['LAST_DATE'] - group_df['FIRST_DATE']).dt.total_seconds() /
        timedelta(days=1).total_seconds()
    )

    # Find those groups where the conditions are met and return the original
    # observations that are associated with those groups
    conds = (
        (group_df.NEAR_ZERO == 1) &
        (group_df.DIFF_DAYS >= args.num_days)
    )
    snow_groups = group_df[conds].GROUP
    return np.where(df.GROUP.isin(snow_groups), 1, 0)


def temperature_spike(df, args):
    """
    Track when series of records spikes for a certain number of periods
    """
    window, threshold = args.window, args.absolute_magnitude
    med_df = df.TEMP_C.rolling(window=window, center=True).median()
    return np.where(np.abs(df.TEMP_C - med_df) >= threshold, 1, 0)


def temperature_jump(df, args):
    """
    Track when current temperature deviates from previous or next record by
    the amount specified in args
    """
    prev_temp_diff = df.TEMP_C.diff()
    next_temp_diff = df.TEMP_C.diff().shift(-1)
    conds = (
        (abs(prev_temp_diff > args.jump_magnitude)) | 
        (abs(next_temp_diff > args.jump_magnitude))
    )
    return np.where(conds, 1, 0)
