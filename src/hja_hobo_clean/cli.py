import sys

import click
from dotmap import DotMap
import yaml

import hja_hobo_clean
from hja_hobo_clean.main import process


@click.command()
@click.argument(
    'configuration-file',
    type=click.Path(exists=True),
    required=True
)
@click.option(
    '--raw',
    is_flag=True,
    default=False,
    help='Flag to process raw files',
)
@click.version_option(
    version=hja_hobo_clean.__version__,
    message='%(version)s'
)
def main(configuration_file, raw):
    # Read in the YAML configuration file and parse all parameters
    with open(configuration_file) as fh:
        params = DotMap(yaml.safe_load(fh))

    # Run processing
    process(params, process_raw=raw)

if __name__ == "__main__":
    main(sys.argv[1:])