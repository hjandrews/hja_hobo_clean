import logging
import os
from collections import namedtuple
from datetime import datetime
from fnmatch import fnmatch
from os.path import basename, join
from os import walk

from dotmap import DotMap
import numpy as np
import pandas as pd
from scipy.stats import linregress
import yaml

from hja_hobo_clean import process_raw as pr
from hja_hobo_clean import flag_functions as ff

# Output template file names
SITE_TEMPLATE = '{dir}/{site}.csv'
FLAGGED_TEMPLATE = '{dir}/{site}_flagged_{year}_{month}.csv'
CLEANED_TEMPLATE = '{dir}/{site}_cleaned_{year}_{month}.csv'
REF_SITE_TEMPLATE = '{dir}/{site}_{year}_{month}_reformatted.csv'
FILLED_TEMPLATE = '{dir}/{site}_filled_{year}_{month}.csv'

# Timestamp templates
TIME_FRMT_STR = '%H:%M:%S'
DATE_FRMT_STR = '%Y %j'
DATE_TS_STR = '{year} {doy}'

# Format string for regression listings
REGRESSION_FS = '{!s}    y = {: 5f}x + {: 5f}    {:8f}'

# Logging parameters
logging.basicConfig(
    filename='regressions.log',
    filemode='w',
    level=logging.INFO,
    format='%(message)s'
)


# Simple site information extracted from filename - pretty fragile
class SiteInformation:
    def __init__(self, fn):
        self.fn = fn
        self.bn = os.path.basename(os.path.splitext(self.fn)[0])
        self.location = self.bn.split('_')[1]

    @property
    def tree_number(self):
        return int(self.location[:3])

    @property
    def sensor_height(self):
        return float(self.location[3:]) / 10.0

    @property
    def site_name(self):
        return '_'.join(self.bn.split('_')[:2])


def decimal_hours(d):
    return d.hour + (d.minute / 60.0) + (d.second / 3600.0)


def get_daylength_data(
        daylength_fn, start_year=2000, end_year=datetime.now().year):
    # Read in the daylength file - it should have three fields:
    # DOY, SUNRISE, and SUNSET.  DOY is from 1-365 and SUNRISE and SUNSET
    # should be a timestamp (HH:MM:SS) using 24-hour notation
    doy_df = pd.read_csv(daylength_fn)

    def time_ts(row, which):
        ts = pd.to_datetime(row[which], format=TIME_FRMT_STR)
        return decimal_hours(ts)

    def date_ts(row, yr):
        s = DATE_TS_STR.format(year=yr, doy=row.DOY)
        return pd.to_datetime(s, format=DATE_FRMT_STR)

    dfs = []
    for year in range(start_year, end_year + 1):
        # Represent sunrise and sunset as decimal hours
        sunrise = doy_df.apply(time_ts, axis=1, args=('SUNRISE',))
        sunset = doy_df.apply(time_ts, axis=1, args=('SUNSET',))

        # Get the date from year and julian day
        d = doy_df.apply(date_ts, axis=1, args=(year,))

        # Name the series and convert to data frame
        sunrise.rename('SUNRISE', inplace=True)
        sunset.rename('SUNSET', inplace=True)
        d.rename('DATE_TIME', inplace=True)
        df = pd.concat([d, sunrise, sunset], axis=1)
        df.set_index('DATE_TIME', inplace=True)

        # Discard the last record if not a leap year
        if not d.loc[0].is_leap_year:
            df = df.iloc[:-1, :]

        # Add to all data frames
        dfs.append(df)

    # Return all years as data frame
    return pd.concat(dfs)


def get_site_files(root_dir):
    site_fns = []
    for dir_path, sub_dirs, fns in walk(root_dir):
        for fn in fns:
            if not fnmatch(fn, '*filled*') \
                    and not fnmatch(fn, '*cleaned*') \
                    and not fnmatch(fn, '*flagged*') \
                    and not fn.startswith('.'):
                site_fns.append(join(dir_path, fn))
    return site_fns


def get_reference_files(root_dir):
    reference_fns = []
    for dir_path, sub_dirs, fns in walk(root_dir):
        for fn in fns:
            if fnmatch(fn, '*reformatted*.csv'):
                reference_fns.append(join(dir_path, fn))
    return reference_fns


def get_site_information(site_fn):
    bn = os.path.basename(os.path.splitext(site_fn)[0])
    value = (bn.split('_'))[1]
    return bn, int(value[:3]), float(value[3:]) / 10.0


def create_file_names(p, site, end_date):
    templates = (FLAGGED_TEMPLATE, CLEANED_TEMPLATE, REF_SITE_TEMPLATE)
    x = p.paths
    dirs = (x.flagged_directory, x.cleaned_directory, x.reference_directory)
    year, month = end_date.year, end_date.month
    return [
        t.format(dir=d, site=site.bn, year=year, month=month)
        for t, d
        in zip(templates, dirs)
    ]


def read_raw_file(site_fn, daylength_df):
    # Read the input CSV file into a pandas dataframe, name the columns,
    # and remove the unneeded ones
    df = pd.read_csv(site_fn)
    df.columns = ['DATE', 'TIME', 'DATE_TIME', 'TEMP_C', 'INTENSITY']
    df.drop(['DATE', 'TIME'], axis=1, inplace=True)

    # Ensure that the DATE_TIME field is cast correctly
    df.DATE_TIME = pd.to_datetime(df.DATE_TIME, infer_datetime_format=True)

    # Determine whether this record is within daylight hours - important
    # to many flags
    df['DAY'] = pd.to_datetime(
            df.DATE_TIME.dt.date, infer_datetime_format=True)
    df['DECIMAL_HOURS'] = decimal_hours(df.DATE_TIME.dt)
    df = df.merge(daylength_df, left_on='DAY', right_index=True, how='left')
    conds = (
        (df.DECIMAL_HOURS >= df.SUNRISE) &
        (df.DECIMAL_HOURS <= df.SUNSET)
    )
    df['DAYLIGHT'] = np.where(conds, 1, 0)
    return df[['DATE_TIME', 'TEMP_C', 'INTENSITY', 'DAYLIGHT']]


def create_flagged_df(df, p, start_time, end_time):
    # Run through all flagging functions (defined in flag_functions module)
    # and listed in the parameters
    drop_flags = []
    for item in p.flag_functions:
        item = DotMap(item)
        func = getattr(ff, item.function, None)
        df[item.name] = func(df, item.arguments)
        if not item.keep:
            drop_flags.append(item.name)

    # Filter to just the requested start/end dates
    df = df[(df.DATE_TIME >= start_time) & (df.DATE_TIME <= end_time)]

    # Return the data frame
    return df, drop_flags


def create_cleaned_df(flagged_df, drop_flags):
    conds = None
    for fn in drop_flags:
        cond = (flagged_df[fn] == 0)
        if conds is None:
            conds = cond
        else:
            conds &= cond
    return flagged_df[conds].loc[:, ['DATE_TIME', 'TEMP_C', 'INTENSITY']]


def average_hourly_values(cleaned_df):
    grouped = cleaned_df.groupby(pd.Grouper(key='DATE_TIME', freq='H'))
    return grouped.agg({
        'TEMP_C': np.mean,
        'INTENSITY': np.mean,
    }).dropna(how='any')


def regress_reference_data(ref_df, other_df):
    merge_df = other_df.merge(ref_df, left_index=True, right_index=True)
    if len(merge_df) == 0:
        return None
    m, b, r, p, err = linregress(merge_df.TEMP_C_x, merge_df.TEMP_C_y)
    return [m, b, r**2, p, err]


def rank_regressions(ref_df, other_data):
    Regression = namedtuple('Regression',
        ['fn', 'df', 'm', 'b', 'r2', 'p', 'err'])
    regression_list = []
    for d in other_data:
        l = regress_reference_data(ref_df, d.df)
        if l is not None:
            r = Regression(*(list(d) + l))
            regression_list.append(r)

    # custom comparator function
    def regression_key(x):
        return x.r2
    regression_list.sort(key=regression_key, reverse=True)
    return regression_list


def substitute_regression_data(filled_df, regression, site_fn):
    fn, df, m, b = regression.fn, regression.df, regression.m, regression.b
    missing_df = filled_df[filled_df.TEMP_C.isnull()]
    filtered_ref_df = df[df.TEMP_C.notnull()]
    merge_df = missing_df.merge(
            filtered_ref_df, how='inner', left_index=True, right_index=True)
    flag_str = 'regression from ' + fn if site_fn != fn else ''
    if len(merge_df):
        merge_df['TEMP_C'] = m * merge_df['TEMP_C_y'] + b
        merge_df['FLAGS'] = flag_str
        merge_df.drop(['TEMP_C_x', 'TEMP_C_y'], axis=1, inplace=True)
        filled_df = filled_df.combine_first(merge_df)
    return filled_df


def create_filled_file_name(p, fn):
    filled_dir = p.paths.filled_directory
    parts = basename(fn).split('_')
    site = '_'.join(parts[:2])
    year, month = parts[2], parts[3]
    return FILLED_TEMPLATE.format(
        dir=filled_dir, site=site, year=year, month=month)

def process_raw_files(params):
    # Iterate through the raw files and only collect if there is a
    # metadata.yaml file.  This specifies the expected file pattern 
    rfd = params.paths.raw_files_directory
    raw_directories = []
    for d in os.listdir(rfd):
        full = os.path.join(rfd, d)
        if os.path.exists(os.path.join(full, 'metadata.yaml')):
            raw_directories.append(full)

    # Print out how many directories were found
    print(f'Raw directories found: {len(raw_directories)}')

    # Process each directory and write the stitched file to the site
    # directory folder.  The output name is just the directory name with
    # a .csv extension 
    for d in raw_directories:
        with open(os.path.join(d, 'metadata.yaml')) as fh:
            bn = os.path.basename(d)
            print(bn)
            p = yaml.safe_load(fh)
            pattern = os.path.join(d, p['file_name_match'])
            df = pr.combine_raw_files(pattern)
            out_fn = SITE_TEMPLATE.format(
                dir=params.paths.site_directory, site=bn)
            df.to_csv(out_fn, index=False, float_format='%.3f')


def process(params, process_raw=False):
    # Read in the daylength file
    daylength_df = get_daylength_data(params.paths.daylength_fn)

    # Ensure all output directories exist
    output_dirs = [
        params.paths.site_directory,
        params.paths.flagged_directory,
        params.paths.cleaned_directory,
        params.paths.reference_directory,
        params.paths.filled_directory
    ]
    for d in output_dirs:
        if not os.path.exists(d):
            os.makedirs(d)

    if process_raw:
        process_raw_files(params)

    # Get list of site files
    site_fns = get_site_files(params.paths.site_directory)

    # Date limits for filled file
    start_time = datetime.strptime(str(params.time.start_date), '%Y-%m-%d')
    end_time = datetime.strptime(str(params.time.end_date), '%Y-%m-%d')
    end_time = end_time.replace(
        hour=23, minute=59, second=59, microsecond=999999)

    # Create a dictionary that maps reference file name to tree_number
    # and height.  This will be filled in during the following loop
    site_dict = {}

    # Create flagged, cleaned and hourly aggregated data
    for site_fn in site_fns:
        print(site_fn)

        # Read in the file and attach the sunrise, sunset information
        raw_df = read_raw_file(site_fn, daylength_df)

        # Flag data
        flagged_df, drop_flags = create_flagged_df(
            raw_df, params, start_time, end_time)

        # Prune data for bad flags
        cleaned_df = create_cleaned_df(flagged_df, drop_flags)

        # Average hourly values
        ref_df = average_hourly_values(cleaned_df)

        # Extract the tree number and height from the file name - fragile
        site_info = SiteInformation(site_fn)

        # Create output file names
        end_date = raw_df.iloc[-1, :].DATE_TIME
        flagged_fn, cleaned_fn, ref_fn = create_file_names(
            params, site_info, end_date)
        site_dict[os.path.basename(ref_fn)] = site_info 
        flagged_df.to_csv(flagged_fn, index=False, float_format='%.3f')
        cleaned_df.to_csv(cleaned_fn, index=False, float_format='%.3f')
        ref_df.to_csv(ref_fn, index=True, float_format='%.3f')

        # Release memory for these dataframes
        del raw_df
        del flagged_df
        del cleaned_df
        del ref_df

    # Get a list of reference files now that all have been created
    # The reference_data object has both the filename and the dataframe
    reference_fns = get_reference_files(params.paths.reference_directory)
    Reference = namedtuple('Reference', ['fn', 'df'])
    reference_data = []
    for fn in reference_fns:
        df = pd.read_csv(
            fn, index_col='DATE_TIME', usecols=['DATE_TIME', 'TEMP_C'],
            parse_dates=True
        )
        reference_data.append(Reference(os.path.basename(fn), df))

    # Create an empty dataframe based on start_time, end_time
    ts = pd.date_range(start_time, end_time, freq='H')
    template_df = pd.DataFrame({
        'TEMP_C': None,
        'FLAGS': None,
    }, index=ts)
    template_df.index.name = 'DATE_TIME'

    # Now re-loop through the reference data to create regressions and
    # back-fill the data
    for d in reference_data:
        site_info = site_dict[d.fn]

        # Create regressions with all other sites if available
        regressions = rank_regressions(d.df, reference_data)

        # Collect all sensors at the same tree within 10m
        this_tree, this_height = site_info.tree_number, site_info.sensor_height
        matches = []
        for k, v in site_dict.items():
            other_tree, other_height = v.tree_number, v.sensor_height
            if other_tree == this_tree:
                diff = abs(other_height - this_height)
                if diff <= 10.0:
                    matches.append((k, diff))
        if matches:
            # Sort on difference.  We need to have the highest difference
            # come first because we will insert them into the regression list
            # in reverse order so that smallest diff is the last to be inserted
            matches.sort(key=lambda x: x[1], reverse=True)

            # Find current index within regressions and move these
            # elements to the front
            for m in matches:
                idx = [i for (i, r) in enumerate(regressions) if r.fn == m[0]]
                regressions.insert(0, regressions.pop(idx[0]))

        # Print out this information
        logging.info(f'Site: {site_info.site_name}')
        for r in regressions:
            i = site_dict[r.fn]
            logging.info('  ' + REGRESSION_FS.format(
                i.site_name, r.m, r.b, r.r2))
        logging.info('')

        # Create a copy of the empty template df
        filled_df = template_df.copy()

        # Fill data from regressions
        for r in regressions:
            filled_df = substitute_regression_data(filled_df, r, d.fn)
        filled_df = filled_df[['TEMP_C', 'FLAGS']]
        filled_df.fillna({'TEMP_C': 1000}, inplace=True)
        filled_fn = create_filled_file_name(params, d.fn)
        filled_df.to_csv(filled_fn, float_format='%.4f', index=True)
