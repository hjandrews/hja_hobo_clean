import glob

import pandas as pd

# Keywords of columns expected in the raw header.  There are sometimes Spanish
# versions in the header
EXPECTED_COLUMNS = [
    ('Time', 'Tiempo'),
    ('Temp',),
    ('Intensity', 'Intensidad'),
]

KEEP_COLUMNS = [
    'DATE_TIME',
    'TEMP_C',
    'INTENSITY_LUX',
]

def combine_raw_files(pattern_match):
    # Set up empty list to hold temporary dataframes
    dfs = []

    # Get all files that match the prefix
    for fn in glob.glob(pattern_match):
        # Read file into dataframe
        df = pd.read_csv(fn, skiprows=1, encoding="iso-8859-1")
        df = df.iloc[:, 1:4]
        
        # Check that columns "mostly" match the expected
        zipped = zip(EXPECTED_COLUMNS, df.columns)
        for col, (expected, actual) in enumerate(zipped):
            if not any((x in actual) for x in expected):
                msg = 'Expected keyword [{}], got column [{}]'.format(
                    expected, actual)
                # Time and temperature are required.  Raise an error if
                # these are not present
                if col < 2:
                    raise ValueError(msg)

                # Intensity is not always present, if missing, trim down 
                # to just the first two columns and continue
                if col == 2:
                    # TODO: Change to log message
                    # print(msg)
                    df = df.iloc[:,0:2]

        # Verify that we actually have three columns - this can only be
        # missing intensity data, so make a new series now
        if len(df.columns) == 2:
            df[KEEP_COLUMNS[2]] = 0.0

        # Change to standardized column names and append to the master df
        df.columns = KEEP_COLUMNS
        df['DATE_TIME'] = pd.to_datetime(df.DATE_TIME, errors='coerce')
        dfs.append(df)

    # Concatenate all dataframes
    master_df = pd.concat(dfs)

    # Sort by date
    master_df.sort_values(['DATE_TIME'], inplace=True)

    # Convert time to 24-hour
    frmt = '%m/%d/%Y %H:%M'
    master_df['DATE'] = master_df.DATE_TIME.dt.strftime('%m/%d/%Y')
    master_df['TIME'] = master_df.DATE_TIME.dt.strftime('%H:%M')
    master_df['DATE_TIME'] = master_df.DATE_TIME.dt.strftime(frmt)
    master_df = master_df[
        ['DATE', 'TIME', 'DATE_TIME', 'TEMP_C', 'INTENSITY_LUX']]

    # Strip out records with any NaNs
    master_df.dropna(inplace=True)

    # Remove any duplicates
    master_df.drop_duplicates(subset='DATE_TIME', keep='first', inplace=True)
    return master_df
