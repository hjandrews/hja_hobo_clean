# HJA Hobo Clean
This is an open source project, and as such, it is free for you to use for your project! We hope it's useful to you. 
It also has no support, no 
maintenance, and no regular updates. The original developer built it for a specific project and has now moved on to 
another position and other projects. Any updates are from the user community as needed to troubleshoot and fix bugs, or 
to develop enhancements. Please give back by sharing yours!

This repository hosts the code needed to process, flag, and clean HOBO-collected data records from the H.J. Andrews
Experimental Forest. HOBO's are placed at ground level, at different heights on selected trees, or suspended 1.5 m
above the ground and collect both temperature and light intensity data.

This code reads in raw csv files collected from HOBO's, creates anomaly flags for user-defined conditions, removes
user-defined erroneous values, fills missing values from nearby or highly correlated sensors and outputs hourly modeled
temperature data.

See [QA documentation](https://bitbucket.org/hjandrews/hja_hobo_clean/downloads/QA_Prgrm_UserGuide_20200106.pdf)
for use.

For general information about resources for this *project*, this *Python
package*, and this *repository*, see our
[wiki](https://bitbucket.org/hjandrews/hja_hobo_clean/wiki).

## Release Notes

## Known Issues

## Installation
Our [User Guide](https://bitbucket.org/hjandrews/hja_hobo_clean/downloads/QA_Prgrm_UserGuide_20200106.pdf)
has a great explanation of how to install this package into a
*virtual environment*. This is the method we recommend. In short:

```
pip install -U /path/to/hja_hobo_clean-0.1.0-py3-none-any.whl
```
### Download the source code
#### Master code
This package is not registered with PyPi, but you can simply download the
code and a wheel file from our downloads page and install with `pip`:

* [src](https://bitbucket.org/hjandrews/hja_hobo_clean/get/937600166398.zip) page.
* wheel ([.whl](https://bitbucket.org/hjandrews/hja_hobo_clean/downloads/hja_hobo_clean-0.1.0-py3-none-any.whl))

#### Development code
For the latest development version, no wheel is provided and you will need to
be a more experienced programmer:

* Go to the Downloads page
* click on the **Branches** tab.
* click on the **develop** branch.
* download the .zip file

### Clone the git repository
If you plan on using the program repeatedly, it's a good idea to clone this repository to your desktop using Git. This
will allow you to get the most recent changes to the program. It will also allow you to share any changes or
improvements that you make with the rest of the community.

There are great [tutorials](https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-clone) available, but
in brief:

`git clone https://<username>@bitbucket.org/hjandrews/hja_hobo_clean`

## Contribute to the project
We encourage all that use this project to contribute to its success! Science is a collaborative process built on our
collective successes and failures, and it only works if we all contribute. All contributions are made by Pull Request.

The original designer and site administrators no longer actively support or update this project, but will try to review 
pull requests as they can. The status of current pull
requests can be seen [here](https://bitbucket.org/hjandrews/hja_hobo_clean/pull-requests/).

## Run the Program
### User Guide
This program preforms statistical analyses including regression and threshold based error detection.
 A detailed description of the use and processes are available:

 * [QA User Guide](https://bitbucket.org/hjandrews/hja_hobo_clean/downloads/QA_Prgrm_UserGuide_20200106.pdf)
 * [White paper on QA methods](https://bitbucket.org/hjandrews/hja_hobo_clean/downloads/QA_WhitePaper_181012.pdf)

## Dependencies
See requirements.txt

click>=6.7
dotmap>=1.2.20
numpy>=1.13.0
pandas>=0.22.0
pyyaml>=3.12
scipy>=1.0.0

## Trouble getting started
Maybe you're new to Git or Python and are having trouble getting started. None
of this makes sense, and you're not sure what to Google! If this is you, we
have tried to help you out on our  
[wiki](https://bitbucket.org/hjandrews/hja_hobo_clean/wiki).
