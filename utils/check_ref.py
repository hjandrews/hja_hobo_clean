import pandas as pd
import numpy as np
import glob
import os

import colorama
from colorama import Fore, Style

DIR_1 = '../clean/Reference_Sites'
DIR_2 = './Reference_Sites'
THRESHOLD = 0.00001


def diff(x):
    try:
        diff_val = x['from'] - x['to']
    except:
        diff_val = 1
    return diff_val


def compare(fn1, fn2):
    df1 = pd.read_csv(fn1, low_memory=False).fillna('')
    df2 = pd.read_csv(fn2, low_memory=False).fillna('')
    df1.columns = ['DATE_TIME', 'TEMP_C']

    # http://stackoverflow.com/questions/17095101
    ne_stacked = (df1 != df2).stack()
    changed = ne_stacked[ne_stacked]
    changed.index.names = ['ID', 'COL']

    diff_locations = np.where(df1 != df2)
    changed_from = df1.values[diff_locations]
    changed_to = df2.values[diff_locations]
    df = pd.DataFrame({
        'from': changed_from,
        'to': changed_to,
    }, index=changed.index)
    df['diff_val'] = df.apply(diff, axis=1)
    df = df[df.diff_val > THRESHOLD]
    return df


if __name__ == '__main__':
    colorama.init()
    fns = glob.glob(os.path.join(DIR_1, '*.csv'))
    for i, fn1 in enumerate(fns):
        fn2 = os.path.join(DIR_2, os.path.basename(fn1))
        if os.path.exists(fn1) and os.path.exists(fn2):
            print(i + 1, os.path.basename(fn1)),
            df_cmp = compare(fn1, fn2)
            if not df_cmp.empty:
                print(Fore.RED + 'FAILED')
                print('-----')
                print(df_cmp.head())
                print('-----'),
            else:
                print(Fore.GREEN + 'PASSED'),
            print(Style.RESET_ALL)
