import logging
import os
import sys

from setuptools import setup, find_packages

# Set up logging
logging.basicConfig(stream=sys.stderr, level=logging.INFO)
log = logging.getLogger()

setup(
    name='hja-hobo-clean',
    version='0.1.1',
    description='Clean HJ Andrews HOBO records into hourly files',
    url='https://bitbucket.org/hjandrews/hja_hobo_clean',
    long_description='',
    author='Matt Gregory',
    author_email='matt.gregory@oregonstate.edu',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    install_requires=[
        'click',
        'dotmap',
        'numpy',
        'pandas',
        'pyyaml',
        'scipy',
    ],
    extras_require={
        'test': [
            'pytest',
            'pytest-cov',
            'tox'
        ]
    },
    entry_points='''
        [console_scripts]
        hja-hobo-clean=hja_hobo_clean.cli:main
    ''',
)
